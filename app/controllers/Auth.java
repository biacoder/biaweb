/*
 * BiaCode® - http://www.biacode.com
 * GitHub - https://github.com/Biacode/bia-web
 * Copyright (c) 2015
 * By BiaCode Systems.
 * Author Arthur Asatryan <biacoder@gmail.com>
 */

package controllers;

import play.mvc.Controller;
import play.mvc.Result;

/**
 * Authentication page / handler
 * Created by Arthur on 2/14/2015.
 */
public class Auth extends Controller {


    public static Result register() {
        return ok();
    }

    public static Result login() {
        return ok();
    }

}
