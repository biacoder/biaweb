/*
 * BiaCode® - http://www.biacode.com
 * GitHub - https://github.com/Biacode/bia-web
 * Copyright (c) 2015
 * By BiaCode Systems.
 * Author Arthur Asatryan <biacoder@gmail.com>
 */

package controllers;

import play.mvc.Controller;
import play.mvc.Result;
import views.html.Home.index;

/**
 * Home page
 * Created by Arthur on 2/14/2015.
 */
public class Home extends Controller {

    public static Result index() {
        return ok(index.render("ok"));
    }

    public static Result home() {
        return ok(index.render("ok"));
    }

}
